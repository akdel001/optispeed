There are two approaches at this point. One will involve creating 
overlapping segments from a molecule and then looking into shared segments 
between other molecules. Then using LSH, we can give each segment an ID. 
We then can obtain the average/median of these segments and decode them. 
We can then compare this to the original segments.

The coding:

1. The input database
    * This can be stored in a padded array with lengths in a second array.
    * Lastly we have to store the indices for segments for each molecule.
    * Indices can be initiated when we load the database
    * Parameters will be overlap length and segment size
    * This can be a dictionary containing a list of tuples

2. Autoencoder data submitter
    * This will 