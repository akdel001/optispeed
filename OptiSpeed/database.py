import numpy as np
from OptiMap import molecule_struct as ms


class MolBase:
    def __init__(self, mollist: np.ndarray, snr: float=3.5) -> None:
        self.segments = list()
        self.mols = mollist
        self.label_sites = [np.array(ms.MoleculeNoBack(x, snr=snr).nick_coordinates) for x in self.mols]

    def segment_mols(self, length: int, overlap: int) -> list:
        """
        Updates self.segments is a list of tuples of three integers: (start, end, number of nicks)
        :param length: length of segment
        :type length: int
        :param overlap: overlap length of segment
        :type overlap: int
        :return: updates self.segments
        :rtype: list of tuples
        """
        self.segments = list()
        for i in range(self.mols.shape[0]):
            current_mol = self.mols[i]
            mol_segments = list()
            nicks = self.label_sites[i]
            for j in range(0, current_mol.shape[0] - length, length-overlap):
                start = j
                end = j + length
                mol_segments.append((start, end, len([x for x in nicks if (end > x > start)])))
            self.segments.append(mol_segments)
        return self.segments

    def yield_segments_in_mol(self, mol_id, labels=True, length=100):
        if labels:
            start, _, _ = self.segments[mol_id][0]
            _, end, _ = self.segments[mol_id][-1]
            current_labels = self.label_sites[mol_id]
            for l in current_labels:
                l = int(l)
                if l + length < end:
                    yield self.mols[mol_id][l:l+length]
        else:
            for i in range(len(self.segments[mol_id])):
                start, end, _ = self.segments[mol_id][i]
                yield self.mols[mol_id][start: end]

    def get_single_segment_in_mol(self, mol_id, segment_id, with_nicks=False):
        segment = self.segments[mol_id]
        segment_start, segment_end, _ = segment[segment_id]
        if not with_nicks:
            return self.mols[mol_id][segment_start: segment_end]
        else:
            segment_nicks = np.array([int(x) for x in self.label_sites[mol_id] if (segment_end > x > segment_start)]) - int(segment_start)
            sparse_nicks = np.zeros(segment_end-segment_start+2)
            sparse_nicks += 0.0000001
            sparse_nicks[segment_nicks.astype(int)] = 1.
            sparse_nicks[np.abs(segment_nicks.astype(int) - 1)] = 0.85
            sparse_nicks[np.abs(segment_nicks.astype(int) - 2)] = 0.75
            sparse_nicks[np.abs(segment_nicks.astype(int) + 1)] = 0.85
            sparse_nicks[np.abs(segment_nicks.astype(int) + 2)] = 0.75
            return self.mols[mol_id][segment_start: segment_end], sparse_nicks[:-2]


if __name__ == "__main__":
    path_to_mols = "/mnt/local_scratch/akdel001/specific_signals.npy"
    mollist = np.load(path_to_mols)
    snr = 3.2
    mdb = MolBase(mollist[:20], snr=snr)
    segments = mdb.segment_mols(50, 25)
    print(segments[:10])
