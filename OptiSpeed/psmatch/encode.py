from OptiSpeed.database import MolBase
from OptiSpeed.compress import Compressor
from OptiMap import molecule_struct as ms
import torch
import numpy as np
from torch.autograd import Variable


class Encoder(MolBase):
    def __init__(self, model, weights_path, signals, length=50, overlap=25, snr=2.9, log_snr=1.2, log_transform=True):
        MolBase.__init__(self, signals, snr=snr)
        self.segment_mols(length, overlap)
        if log_transform:
            self.mols = [ms.Molecule(x, x, snr=snr, log_snr=log_snr).log_signal for x in self.mols]
        self.overlap = overlap
        self.length = length
        self.model = model(length=length)
        if weights_path is "":
            pass
        else:
            self.model.load_state_dict(torch.load(weights_path))
            self.model.eval()
        self.encoded_segments = list()
        self.assigned_segments = list()
        self.segment_indices = list()
        self.segment_count = 0
        self.unencoded_segments = None

    def encode_segments(self, labels=True):
        for i in range(len(self.mols)):
            unencoded_segments = np.array(list(self.yield_segments_in_mol(i, labels=labels)))
            if unencoded_segments.shape[0]:
                self.segment_count += unencoded_segments.shape[0]
                self.encoded_segments.append(self.model.encoder(Variable(
                        torch.from_numpy(unencoded_segments.astype("float32").reshape((unencoded_segments.shape[0], 1, -1)))
                    )).detach().cpu().numpy().reshape((unencoded_segments.shape[0], -1)))
            else:
                self.encoded_segments.append(self.model.encoder(Variable(
                        torch.from_numpy(np.zeros(self.length).astype("float32").reshape((1, 1, -1)))
                    )).detach().cpu().numpy().reshape((1, -1)))
            if (i % 500) == 0:
                    print(i)

    def segments_without_encoding(self):
        for i in range(len(self.mols)):
            unencoded_segments = np.array(list(self.yield_segments_in_mol(i, labels=True, length=self.length)))
            self.segment_count += unencoded_segments.shape[0]
            self.encoded_segments.append((unencoded_segments))


    # def encode_segments_v2(self, parts=10000):
    #     for i in range(len(self.mols)):
    #         seg_len = len(self.segment_indices[i])
    #         current_segments = np.zeros((seg_len, self.length))
    #         for segment in self.yield_segments_in_mol(i):
    #             self.segment_count += 1
    #             current_segments[]
    #             current_encoded_segments.append(self.model.encoder(Variable(
    #                 torch.from_numpy(segment.astype("float32").reshape((1, 1, -1)))
    #             )).detach().cpu().numpy().flatten())
    #         self.encoded_segments.append(current_encoded_segments)
    #         if (i % 500) == 0:
    #             print(i)

    def load_segments(self, filename):
        self.encoded_segments = list(np.load(filename))

    def _count_segments(self):
        self.segment_count = 0
        cumulative = 0
        for i in range(len(self.encoded_segments)):
            self.assigned_segments.append((cumulative, cumulative+len(self.encoded_segments[i])))
            cumulative += len(self.encoded_segments[i])
            self.segment_count += len(self.encoded_segments[i])

    def _yield_encoded_segments(self):
        for i in range(len(self.encoded_segments)):
            for j in range(len(self.encoded_segments[i])):
                yield self.encoded_segments[i][j]

    def get_full_encoded_segment_array(self):
        if not self.segment_count:
            self._count_segments()
        encoded_segment_length = self.encoded_segments[0][0].shape[0]
        res = np.zeros((self.segment_count, encoded_segment_length))
        i = 0
        for encoded_segment in self._yield_encoded_segments():
            res[i] = encoded_segment
            i += 1
        return res

    def decode_segment(self, segment):
        return self.model.decoder(Variable(torch.from_numpy(segment.astype("float32").reshape((1, 1, -1))
                                                            ))).detach().cpu().numpy().flatten()

    def encode_segment(self, segment):
        return self.model.encoder(Variable(torch.from_numpy(segment.astype("float32").reshape((1, 1, -1))
                                                            ))).detach().cpu().numpy().flatten()

if __name__ == "__main__":
    path_to_mols = "/mnt/local_scratch/akdel001/specific_signals.npy"
    mollist = np.load(path_to_mols)[:50000]
    snr = 3.2
    enc = Encoder(Compressor, "/mnt/local_scratch/akdel001/OptiSpeed/output/models/model1", mollist)
    enc.encode_segments()
    np.save("/mnt/local_scratch/akdel001/OptiSpeed/output/encoded_segments2.npy", enc.encoded_segments)
    # enc.load_segments("/mnt/local_scratch/akdel001/OptiSpeed/output/encoded_segments.npy")
    # arr = enc.get_full_encoded_segment_array()
    # print(arr[:10])
    # print(enc.assigned_segments[:10])



