import numpy as np
import numba as nb
from OptiMap import align


def mols_to_mollens(mols):
    lens = np.array([x.shape[0] for x in mols], dtype=int)
    res = np.zeros((len(mols), np.max(lens)))
    for i in range(len(mols)):
        res[i][:lens[i]] = mols[i]
    return res, lens


@nb.njit(parallel=True)
def get_alignments_with_optimap(mol, rest, lens, limit=250):
    res = np.zeros(rest.shape[0])
    for i in nb.prange(res.shape[0]):
        current_mol = np.zeros(rest[i][:lens[i]].shape)
        current_mol[:] = rest[i][:lens[i]]
        if current_mol.shape[0] > mol.shape[0]:
            f = np.max(align.normalized_correlation(current_mol, mol, limit=limit))
        else:
            f = np.max(align.normalized_correlation(mol, current_mol, limit=limit))
        res[i] = f
    return res


@nb.njit(parallel=True)
def get_filtered_alignments(mol, prerest, prelens, filter_ids, limit=250):
    res = np.zeros((filter_ids.shape[0], 2))
    rest = prerest[filter_ids]
    lens = prelens[filter_ids]
    for i in nb.prange(res.shape[0]):
        current_mol = np.zeros(rest[i][:lens[i]].shape)
        current_mol[:] = rest[i][:lens[i]]
        if current_mol.shape[0] > mol.shape[0]:
            f = np.max(align.normalized_correlation(current_mol, mol, limit=limit))
        else:
            f = np.max(align.normalized_correlation(mol, current_mol, limit=limit))
        res[i, 0] = f
        res[i, 1] = filter_ids[i]

    return res


# @nb.njit
def get_filtered_alignments_all_mols(mols, prerest, prelens, all_filter_ids, limit=250, test_limit=10):
    res = np.zeros((test_limit, all_filter_ids.shape[1], 3))
    for i in range(test_limit):
        filter_ids = all_filter_ids[i]
        current_mol = mols[i]
        res[i,:,:2] = get_filtered_alignments(current_mol, prerest, prelens, filter_ids, limit=limit)
        res[i, :, -1] = i
    return res

def get_alignments_all_mols(mols, prerest, prelens, limit=250, test_limit=10):
    res = np.zeros((test_limit, prelens.shape[0], 3))
    for i in range(test_limit):
        filter_ids = np.arange(prelens.shape[0])
        current_mol = mols[i]
        res[i,:,:2] = get_filtered_alignments(current_mol, prerest, prelens, filter_ids, limit=limit)
        res[i, :, -1] = i
    return res

def return_argsorted_top(mat, score_lim=0.6):
    pairs = set()
    for i in range(mat.shape[0]):
        for j in range(mat.shape[1]):
            if (i < j) and mat[i, j, 0] >= score_lim:
                pairs.add((i, j))
    return pairs


@nb.njit(parallel=True)
def match_pairs_by_optimap(pairs, padded_logmols, lens, min_ovl=250):
    res = np.zeros(pairs.shape[0])
    for i in nb.prange(res.shape[0]):
        p1 = pairs[i][0]
        p2 = pairs[i][1]
        current_mol1 = padded_logmols[p1][:lens[p1]]
        current_mol2 = padded_logmols[p2][:lens[p2]]
        if current_mol1.shape[0] < min_ovl or current_mol2.shape[0] < min_ovl:
            res[i] = 0
            continue
        if current_mol1.shape[0] > current_mol2.shape[0]:
            f = max(np.max(align.normalized_correlation(current_mol1, current_mol2, limit=min_ovl)),
                    np.max(align.normalized_correlation(current_mol1, current_mol2[::-1], limit=min_ovl)))
        else:
            f = max(np.max(align.normalized_correlation(current_mol2, current_mol1, limit=min_ovl)),
                    np.max(align.normalized_correlation(current_mol2, current_mol1[::-1], limit=min_ovl)))
        res[i] = f
    return res


@nb.njit(parallel=True)
def match_pairs_by_optimap_norev(pairs, padded_logmols, lens, min_ovl=250):
    res = np.zeros(pairs.shape[0])
    for i in nb.prange(res.shape[0]):
        p1 = pairs[i][0]
        p2 = pairs[i][1]
        current_mol1 = padded_logmols[p1][:lens[p1]]
        current_mol2 = padded_logmols[p2][:lens[p2]]
        if current_mol1.shape[0] < min_ovl or current_mol2.shape[0] < min_ovl:
            res[i] = 0
            continue
        if current_mol1.shape[0] > current_mol2.shape[0]:
            f = np.max(align.normalized_correlation(current_mol1, current_mol2, limit=min_ovl))
        else:
            f = np.max(align.normalized_correlation(current_mol2, current_mol1, limit=min_ovl))
        res[i] = f
    return res


@nb.njit
def euc(arr1, arr2):
    res = 0
    for i in range(arr1.shape[0]):
        res += (arr1[i]-arr2[i])**2
    return np.sqrt(res)


@nb.njit(parallel=True)
def get_euc_distances_between_pair(mol_segments1, mol_segments2):
    res = np.zeros(mol_segments1.shape)
    for i in nb.prange(mol_segments1.shape[0]):
        current_mins = np.zeros(mol_segments2.shape[0])
        a = mol_segments1[i]
        for j in range(mol_segments2.shape[0]):
            b = mol_segments2[j]
            r = np.sum(a * b)/np.sqrt((np.sum(a**2) * np.sum(b**2)))
            if r > 0.96:
                current_mins[j] = 1
                break
                # current_mins[j] = 1
        res[i] = np.max(current_mins)
    return res


def one_vs_all_euc_sums(mol1_segments, rest_of_segments):
    res = np.zeros(len(rest_of_segments))
    for i in range(res.shape[0]):
        res[i] = np.mean(get_euc_distances_between_pair(np.array(mol1_segments).astype(float),
                                                np.array(rest_of_segments[i]).astype(float)))
    return res


def compare_real_alignments_with_euc(mol, mol_segments, rest, rest_mol_segments):
    pass
