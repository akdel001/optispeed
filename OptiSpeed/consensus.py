from OptiAsm import string_graph as sg
import intervaltree as it
import numpy as np
import numba


class Consensus:
    def __init__(self, corrs):
        self.space = it.IntervalTree()
        self.corrs = corrs
        self.index = dict()
        self.consensus_signal = None
        self.max_interval = 50
        self.used = set()

    def get_corrs_from_pair(self, pair):
        filt_corrs1 = self.corrs[self.corrs["long_id"] == pair[0]]
        filt_corrs1 = filt_corrs1[filt_corrs1["short_id"] == pair[1]]
        filt_corrs2 = self.corrs[self.corrs["short_id"] == pair[0]]
        filt_corrs2 = filt_corrs2[filt_corrs2["long_id"] == pair[1]]
        return np.concatenate((filt_corrs1, filt_corrs2))

    def from_path_to_space(self, path):
        path_piece1 = path[:-1]
        path_piece2 = path[1:]
        corrs = np.zeros(1, dtype=sg.DTYPE).reshape((1,))
        for pair in zip(path_piece1, path_piece2):
            corrs = np.concatenate((corrs, self.get_corrs_from_pair(pair)))
        edges = corrs[1:]
        graph = sg.StringGraph(edges.reshape((-1, 1)))
        graph.create_graph_v4()
        pad = 0
        for pair in zip(path_piece1, path_piece2):
            graph_edge = graph.graph2[pair[0]][pair[1]]
            if pair[0] == graph_edge["short"]:
                if graph_edge["type"][0] == True:
                    self.space[pad:pad+graph_edge["short_len"]] = (pair[0], graph_edge["type"][0])
                    self.space[pad + min(graph_edge["overlap"][0]):pad + min(graph_edge["overlap"][0]) + graph_edge["long_len"]] = (pair[1],graph_edge["type"][1])
                    pad = pad + min(graph_edge["overlap"][0])
                else:
                    self.space[pad:pad+graph_edge["short_len"]] = (pair[0], graph_edge["type"][0])
                    self.space[pad +(graph_edge["short_len"]- min(graph_edge["overlap"][1])):pad +(graph_edge["short_len"]- min(graph_edge["overlap"][1])) + graph_edge["long_len"]] = (pair[1],graph_edge["type"][1])
                    pad = pad + (graph_edge["short_len"] - max(graph_edge["overlap"][0]))
            else:
                if graph_edge["type"][0] == True:
                    self.space[pad:pad+graph_edge["long_len"]] = (pair[0], graph_edge["type"][0])
                    self.space[pad + min(graph_edge["overlap"][0]):pad + min(graph_edge["overlap"][0]) + graph_edge["short_len"]] = (pair[1],graph_edge["type"][1])
                    pad = pad + min(graph_edge["overlap"][0])
                else:
                    self.space[pad:pad+graph_edge["long_len"]] = (pair[0], graph_edge["type"][0])
                    self.space[pad +(graph_edge["long_len"]- min(graph_edge["overlap"][1])):pad +(graph_edge["long_len"]- min(graph_edge["overlap"][1])) + graph_edge["short_len"]] = (pair[1],graph_edge["type"][1])
                    pad = pad + (graph_edge["long_len"] - max(graph_edge["overlap"][0]))
            # print(pad)
        self.indices_from_space()

    # def from_path_to_space(self, path):
    #     path_piece1 = path[:-1]
    #     path_piece2 = path[1:]
    #     pad = 0
    #     for pair in zip(path_piece1, path_piece2):
    #         corr = self.get_corrs_from_pair(pair)[0]
    #         # if pair[0] == corr["long_id"]:
    #         # if pair[0] == corr["long_id"]:
    #         #     p1 = pair[0]
    #         #     p2 = pair[1]
    #         # else:
    #         #     p1 = pair[1]
    #         #     p2 = pair[0]
    #
    #         if corr["long_start"] != 0:
    #             self.space[pad:pad + corr["long_len"]] = (corr["long_id"], False)
    #             if not corr["reversed"]:
    #                 self.space[pad+corr["long_start"]:pad+corr["long_start"]+corr["short_len"]] = (corr["short_id"], False)
    #             else:
    #                 self.space[pad + corr["long_start"]:pad + corr["long_start"] + corr["short_len"]] = (corr["short_id"], True)
    #             pad += corr["long_start"]
    #         else:
    #             if not corr["reversed"]:
    #                 self.space[pad:pad + corr["short_len"]] = (corr["short_id"], False)
    #             else:
    #                 self.space[pad:pad + corr["short_len"]] = (corr["short_id"], True)
    #             self.space[pad+corr["short_start"]:pad+corr["short_start"]+corr["long_len"]] = (corr["long_id"], True)
    #             pad += corr["short_start"]
    #     self.indices_from_space()

    def indices_from_space(self):
        for inter in self.space.all_intervals:
            self.index[inter.data[0]] = (inter.data[1], inter.begin, inter.end)

    def find_contained_mols(self, mol_id):
        for corr in self.corrs:
            if mol_id == corr["long_id"] or mol_id == corr["short_id"]:
                if corr["contained"] == True and corr["long_id"] == mol_id:
                    yield corr["short_id"], corr["reversed"], 1., (corr["long_start"], corr["long_end"])

    def find_containing_mols(self, mol_id, thr=0.7):
        for corr in self.corrs:
            if corr["overlap_score"] >= thr:
                if mol_id == corr["short_id"] or mol_id == ["long_id"]:
                    if corr["contained"] == True and corr["short_id"] == mol_id:
                        if corr["reversed"]:
                            containing_reversed = True
                        else:
                            containing_reversed = False
                        yield corr["long_id"], containing_reversed, 1., (corr["long_start"],corr["long_end"])

    def fill_contained_space(self, path, mols, containing=False):
        for mol_id in [x for x in path if x not in self.used]:
            for cont_id,reverse,zoom,long_overlap in self.find_contained_mols(mol_id):
                if cont_id in self.index:
                    continue
                else:
                    if reverse and (self.index[mol_id][0] == True):
                        reverse = False
                        start = min(self.index[mol_id][1:]) + min(long_overlap)
                        end = start + mols[cont_id].shape[0]
                    elif not reverse and self.index[mol_id][0] == True:
                        reverse = True
                        start = min(self.index[mol_id][1:]) + min(long_overlap)
                        end = start + mols[cont_id].shape[0]
                    elif reverse and self.index[mol_id][0] == False:
                        end = max(self.index[mol_id][1:]) - min(long_overlap)
                        start = end - mols[cont_id].shape[0]
                    elif not reverse and self.index[mol_id][0] == False:
                        end = max(self.index[mol_id][1:]) - min(long_overlap)
                        start = end - mols[cont_id].shape[0]
                    else:
                        # print(reverse, self.index[mol_id])
                        continue
                    self.space[start:end] = (cont_id, reverse)
                    self.index[cont_id] = (reverse, start, end)
            if containing:
                for cont_id, reverse, zoom, long_overlap in self.find_containing_mols(mol_id):
                    if cont_id in self.index:
                        continue
                    else:
                        if reverse and self.index[mol_id][0] == True:
                            reverse = False
                            start = min(self.index[mol_id][1:]) - (mols[cont_id].shape[0] - max(long_overlap))
                            end = start + mols[cont_id].shape[0]
                        elif not reverse and self.index[mol_id][0] == True:
                            reverse = True
                            start = min(self.index[mol_id][1:]) - min(long_overlap)
                            end = start + mols[cont_id].shape[0]
                        elif reverse and self.index[mol_id][0] == False:
                            reverse = True
                            start = min(self.index[mol_id][1:]) - min(long_overlap)
                            end = start + mols[cont_id].shape[0]
                        elif not reverse and self.index[mol_id][0] == False:
                            reverse = False
                            start = min(self.index[mol_id][1:]) - (mols[cont_id].shape[0] - max(long_overlap))
                            end = start + mols[cont_id].shape[0]
                        else:
                            continue
                        if end <= self.max_interval:
                            self.space[start:end] = (cont_id, reverse)
                            self.index[cont_id] = (reverse, start, end)
                        else:
                            pass
        self.used |= set(path)

    def from_path_to_space2(self, path):
            path_piece1 = path[:-1]
            path_piece2 = path[1:]
            pad = 0
            corrs = np.zeros(1, dtype=sg.DTYPE).reshape((1,))
            for pair in zip(path_piece1, path_piece2):
                corrs = np.concatenate((corrs, self.get_corrs_from_pair(pair)))
            edges = corrs[1:]
            graph = sg.StringGraph(edges.reshape((-1, 1)))
            graph.create_graph_v4()
            for pair in zip(path_piece1, path_piece2):
                graph_edge = graph.graph2[pair[0]][pair[1]]
                if pair[0] == graph_edge["short"]:
                    if graph_edge["type"][0] == True:
                        self.space[pad:pad+graph_edge["short_len"]] = (pair[0], graph_edge["type"][0])
                        self.space[pad + min(graph_edge["overlap"][0]):pad + min(graph_edge["overlap"][0]) + graph_edge["long_len"]] = (pair[1],graph_edge["type"][1])
                        pad = pad + min(graph_edge["overlap"][0])
                    else:
                        self.space[pad:pad+graph_edge["short_len"]] = (pair[0], graph_edge["type"][0])
                        self.space[pad +(graph_edge["short_len"]- max(graph_edge["overlap"][0])):pad +(graph_edge["short_len"]- max(graph_edge["overlap"][0])) + graph_edge["long_len"]] = (pair[1],graph_edge["type"][1])
                        pad = pad + (graph_edge["short_len"]- max(graph_edge["overlap"][0]))
                else:
                    if graph_edge["type"][0] == True:
                        self.space[pad:pad+graph_edge["long_len"]] = (pair[0], graph_edge["type"][0])
                        self.space[pad + min(graph_edge["overlap"][0]):pad + min(graph_edge["overlap"][0]) + graph_edge["short_len"]] = (pair[1],graph_edge["type"][1])
                        pad = pad + min(graph_edge["overlap"][0])
                    else:
                        self.space[pad:pad+graph_edge["long_len"]] = (pair[0], graph_edge["type"][0])
                        self.space[pad +(graph_edge["long_len"]- max(graph_edge["overlap"][0])):pad +(graph_edge["long_len"]- max(graph_edge["overlap"][0])) + graph_edge["short_len"]] = (pair[1],graph_edge["type"][1])
                        pad = pad + (graph_edge["long_len"]- max(graph_edge["overlap"][0]))
            self.indices_from_space()
            self.max_interval += max([x.end for x in self.space.all_intervals])


def fill_in_array(indices, mols):
    indices = list(indices.items())
    maxim = max(indices, key=lambda x: x[1][2])[1][2]
    res = np.zeros((len(indices), int(maxim+100)))
    res[:] = np.nan
    n = 0
    for i, (reversed, start, end) in sorted(indices, key=lambda x: x[1][2]):
        if not reversed:
            mol = mols[i][::-1]
        else:
            mol = mols[i]
        res[n, int(start):int(start+mol.shape[0])] = mol
        n+=1
    return res



@numba.jit
def move_right(arr):
    arr[1:] = arr[:-1]
    arr[0] = 0
    return arr

@numba.jit
def move_left(arr):
    arr[:-1] = arr[1:]
    arr[-1] = 0
    return arr

@numba.vectorize
def nan_to_1(num):
    if np.isnan(num) or num <= 0:
        return 0
    else:
        return num

@numba.jit
def get_position(arr,inner):
    res = np.zeros(5)
    current = np.zeros(inner.shape)
    current[:] = nan_to_1(inner)
    j = 1
    for i in range(2):
        current = move_right(current)
        res[j] = np.sum(current*nan_to_1(arr))
        j -= 1
    current[:] = nan_to_1(inner)
    res[2] = np.sum(nan_to_1(inner)*nan_to_1(arr))
    j += 1
    for i in range(2):
        current = move_left(current)
        res[j] = np.nansum(current*nan_to_1(arr))
        j += 1
    return res

@numba.jit(parallel=True)
def nanmed(arr):
    res = np.zeros(arr.shape[1])
    t_arr = np.transpose(arr)
    for i in numba.prange(t_arr.shape[0]):
        res[i] = np.nanmean(t_arr[i])
    return res

@numba.jit
def optimize_consensus(arr, n_iter):
    saved = np.zeros(arr.shape[1])
    for i in range(n_iter):
        for j in range(arr.shape[0]):
            saved[:] = arr[j,:]
            arr[j,:] = np.nan
            medarr = nanmed(arr)
            pos = np.argmax(get_position(medarr, saved)) - 2
            if pos < 0:
                arr[j,:pos] = saved[-1*pos:]
            elif pos > 0:
                arr[pos:] = saved[:-1*pos]
            else:
                arr[j,:] = saved[:]
    return arr
