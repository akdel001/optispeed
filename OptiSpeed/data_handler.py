from torch.utils.data import DataLoader, Dataset
import numpy as np
import random
from OptiSpeed.database import MolBase
from OptiMap import molecule_struct as ms
import torch


class SegmentProvider(Dataset, MolBase):
    def __init__(self, mollist, length, overlap, log_transform=False, snr=3.5, log_snr=1.2):
        MolBase.__init__(self, mollist, snr=snr)
        self.segment_mols(length, overlap)
        if log_transform:
            self.mols = [ms.Molecule(x, x, snr=snr, log_snr=log_snr).log_signal for x in self.mols]
        self.overlap = overlap
        self.length = length

    def replace_mols(self, filename):
        new_mols = np.load(filename)
        if new_mols.shape[0] == len(self.mols):
            self.mols = new_mols
        else:
            print("Molecules could not be replaced due to the shape difference.")

    def __getitem__(self, index):
        mol_id = int(random.randrange(0, len(self.mols)))
        number_of_segments = len(self.segments[mol_id])
        random_segment_id = int(random.randrange(0, number_of_segments))
        return torch.from_numpy(self.get_single_segment_in_mol(mol_id,
                                                               random_segment_id).astype("float32").reshape((1, -1)))

    def __len__(self):
        return len(self.mols)


class BiSegmentProvider(SegmentProvider):
    def __init__(self, mollist, length, overlap, log_transform=False, snr=3.5, log_snr=1.2):
        SegmentProvider.__init__(self, mollist, length, overlap, log_transform=log_transform, snr=snr, log_snr=log_snr)

    def __getitem__(self, index):
        mol_id = int(random.randrange(0, len(self.mols)))
        number_of_segments = len(self.segments[mol_id])
        random_segment_id = int(random.randrange(0, number_of_segments))
        seg, nicks = self.get_single_segment_in_mol(mol_id, random_segment_id, with_nicks=True)
        seg += 0.00000001
        seg /= 6
        seg = torch.from_numpy(seg.astype("float32").reshape((1, -1)))
        nicks = torch.from_numpy(nicks.astype("float32").reshape((1, -1)))
        return seg, nicks


def segment_loader(segment_provider, shuffle: bool, number_of_workers: int, batch_size: int):
    return DataLoader(segment_provider,
                      shuffle=shuffle,
                      num_workers=number_of_workers,
                      batch_size=batch_size)


if __name__ == "__main__":
    path_to_mols = "/mnt/local_scratch/akdel001/specific_signals.npy"
    mollist = np.load(path_to_mols)[:10]
    snr = 3.2
    segment_provider = SegmentProvider(mollist, 50, 25, snr=3.5)
    print(segment_provider[0])
    print(segment_provider[0])

