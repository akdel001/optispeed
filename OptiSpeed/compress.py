from OptiSpeed import data_handler as dh



PLOT_PATH = "/mnt/local_scratch/akdel001/OptiSpeed/output/plots/"
MODEL_PATH = "/mnt/local_scratch/akdel001/OptiSpeed/output/models/"


class BiLinearWithNicks(dh.torch.nn.Module):
    def __init__(self, length=50, m=1):
        super(BiLinearWithNicks, self).__init__()
        self.length = length
        self.m = m
        # self.bilinear = dh.torch.nn.Bilinear(self.length, self.length, self.length * 2)
        self.encoder = dh.torch.nn.Sequential(
            dh.torch.nn.Linear(length, length * 2),
            dh.torch.nn.ConvTranspose1d(1, 32, 10),
            dh.torch.nn.ReLU(True),
            dh.torch.nn.ConvTranspose1d(32, 1, 10),
            dh.torch.nn.Tanh(),
            dh.torch.nn.Linear(length * 2 + 18, int(length * 0.7)),
            dh.torch.nn.Linear(int(length * 0.7), int(length * 0.4)),
            dh.torch.nn.Linear(int(length * 0.4), int(length * 0.15))
        )

        self.decoder = dh.torch.nn.Sequential(
            dh.torch.nn.Linear(int(length * 0.15), int(length * 0.4)),
            dh.torch.nn.Linear(int(length * 0.4), int(length * 0.7)),
            dh.torch.nn.Linear(int(length * 0.7), length)
        )

    def forward(self, x):
        # x = self.bilinear(x, y)
        x = self.encoder(x)
        x = self.decoder(x)
        return x.clamp(min=self.m)


class LinearCompressor(dh.torch.nn.Module):
    def __init__(self, length=50, m=0):
        super(LinearCompressor, self).__init__()
        self.m = m
        self.encoder = dh.torch.nn.Sequential(
            dh.torch.nn.Linear(length, length * 2),
            dh.torch.nn.ConvTranspose1d(1, 32, 10),
            dh.torch.nn.ReLU(True),
            dh.torch.nn.ConvTranspose1d(32, 1, 10),
            dh.torch.nn.Tanh(),
            dh.torch.nn.Linear(length * 2 + 18, int(length * 0.7)),
            dh.torch.nn.Linear(int(length * 0.7), int(length * 0.4)),
            dh.torch.nn.Linear(int(length * 0.4), int(length * 0.15))
        )

        self.decoder = dh.torch.nn.Sequential(
            dh.torch.nn.Linear(int(length * 0.15), int(length * 0.4)),
            dh.torch.nn.Linear(int(length * 0.4), int(length * 0.7)),
            dh.torch.nn.Linear(int(length * 0.7), length)
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x.clamp(min=self.m)


class SimpleCompressor(dh.torch.nn.Module):
    def __init__(self, length=50, m=0):
        super(SimpleCompressor, self).__init__()
        self.m = m
        self.encoder = dh.torch.nn.Sequential(
            dh.torch.nn.Linear(length, length*2),
            dh.torch.nn.ConstantPad1d(4, 0),
            dh.torch.nn.Conv1d(1, 32, 5),
            dh.torch.nn.ReLU(True),
            dh.torch.nn.MaxPool1d(2),
            dh.torch.nn.Conv1d(32, 1, 3),
            dh.torch.nn.ReLU(True),
            dh.torch.nn.Linear(length, int(length * 0.5)),
            dh.torch.nn.Linear(int(length * 0.5), int(length * 0.15)),
        )

        self.decoder = dh.torch.nn.Sequential(
            dh.torch.nn.Linear(int(length * 0.15), int(length)),
            dh.torch.nn.ConvTranspose1d(1, 32, 3),
            dh.torch.nn.ReLU(True),
            dh.torch.nn.ConvTranspose1d(32, 1, 3),
            dh.torch.nn.Tanh(),
            dh.torch.nn.Linear(length + 4, length)
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x.clamp(min=self.m)


class Compressor(dh.torch.nn.Module):
    def __init__(self, length=50, m=0):
        super(Compressor, self).__init__()
        self.m = m
        self.encoder = dh.torch.nn.Sequential(
            dh.torch.nn.ConstantPad1d(13, 0),
            dh.torch.nn.Conv1d(1, 32, 3),
            dh.torch.nn.ReLU(True),
            dh.torch.nn.MaxPool1d(3),
            dh.torch.nn.Conv1d(32, 32, 3),
            dh.torch.nn.ReLU(True),
            dh.torch.nn.MaxPool1d(3),
            dh.torch.nn.Conv1d(32, 1, 3),
            dh.torch.nn.ReLU(True),
        )

        self.decoder = dh.torch.nn.Sequential(
            dh.torch.nn.Linear(length//9, length*2),
            dh.torch.nn.ConvTranspose1d(1, 32, 3),
            dh.torch.nn.ReLU(True),
            dh.torch.nn.ConvTranspose1d(32, 32, 3),
            dh.torch.nn.ReLU(True),
            dh.torch.nn.ConvTranspose1d(32, 1, 3),
            dh.torch.nn.Tanh(),
            dh.torch.nn.Linear(length*2 + 6, length))

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x.clamp(min=self.m)



def loss_function(arr1, arr2):
    return ((1 - dh.torch.sum(
        (arr1 * arr2)
    ) / dh.torch.sqrt(
        dh.torch.sum(dh.torch.pow(arr1, 2)) * dh.torch.sum(dh.torch.pow(arr2, 2))
    )) * 10) ** 3


def trainer(model, optimizer, number_of_epochs, train_dataloader, bilinear=False):
    import matplotlib
    matplotlib.use("agg")
    import matplotlib.pyplot as plt

    counter = []
    print(f"Total batches: {len(train_dataloader)}\nNumber of signals per batch: {train_dataloader.batch_size}")
    for epoch in range(number_of_epochs):
        loss_avg = 0.
        n = 0
        for data in train_dataloader:
            signal = data
            if not bilinear:
                signal = dh.torch.autograd.Variable(signal).cpu()
                decompressed = model(signal)
                loss = loss_function(decompressed, signal)
            else:
                signal, nicks = signal
                signal = dh.torch.autograd.Variable(signal).cpu()
                nicks = dh.torch.autograd.Variable(nicks).cpu()
                decompressed = model(signal)
                loss = loss_function(decompressed, nicks)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            loss_avg += float(loss.data)
            n += 1
        print('epoch [{}/{}], loss:{:.4f}'
              .format(epoch + 1, number_of_epochs, float(loss.data)))
        counter.append(loss_avg / n)
        if epoch % 1 == 0:
            if bilinear:
                plt.plot(nicks[0, 0, :].detach().cpu().numpy().flatten())
                plt.plot(signal[0, 0, :].detach().cpu().numpy().flatten())
            else:
                plt.plot(signal[0, 0, :].detach().cpu().numpy().flatten())
            plt.plot(decompressed[0, 0, :].detach().cpu().numpy().flatten())
            plt.savefig(f"{PLOT_PATH}signals_{epoch}.png")
            plt.cla()
            plt.plot(counter)
            plt.savefig(f"{PLOT_PATH}loss.png")
            plt.cla()
    return model


if __name__ == "__main__":
    path_to_mols = "/mnt/local_scratch/akdel001/specific_signals.npy"
    path_to_log_mols = "/mnt/local_scratch/akdel001/specific_log_signals.npy"
    mollist = dh.np.load(path_to_mols)[:30000]
    snr = 3.2
    segment_data = dh.SegmentProvider(mollist, 50, 25, log_transform=True, snr=3.5)
    # segment_data.replace_mols(path_to_log_mols)
    segment_provider = dh.segment_loader(segment_data, True, 38, 250)
    model = Compressor(length=50).cpu()
    optimizer = dh.torch.optim.Adam(model.parameters(), lr=1e-3)
    epochs = 250
    model = trainer(model, optimizer, epochs, segment_provider)
    dh.torch.save(model.state_dict(), MODEL_PATH + "model2_30000mols")
