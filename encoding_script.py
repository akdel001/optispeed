from OptiSpeed.compress import Compressor, trainer, MODEL_PATH, SimpleCompressor, LinearCompressor, BiLinearWithNicks
from OptiSpeed.psmatch.encode import Encoder
from OptiSpeed import data_handler as dh
import numpy as np


def main():
    # path_to_mols = "/mnt/local_scratch/akdel001/specific_signals.npy"
    path_to_mols = "/mnt/nexenta/akdel001/phd/Data/eggplant/molecules/run5/run5_mols.npy"
    mollist = np.load(path_to_mols)[:50000]
    # snr = 3.2
    # backbones = np.load("/mnt/local_scratch/akdel001/backbone_signals.npy")[:30000]
    segment_data = dh.SegmentProvider(mollist[:50000], 100, 80, log_transform=True, snr=3.5, log_snr=1.2)
    segment_provider = dh.segment_loader(segment_data, True, 40, 1000)
    model = LinearCompressor(length=100, m=0).cpu()
    optimizer = dh.torch.optim.Adam(model.parameters(), lr=1e-3)
    epochs = 8
    model = trainer(model, optimizer, epochs, segment_provider, bilinear=False)
    dh.torch.save(model.state_dict(), MODEL_PATH + "eggplant_model")


def main_bilinear():
    print("bilinear")
    path_to_mols = "/mnt/local_scratch/akdel001/specific_signals.npy"
    mollist = np.load(path_to_mols)[:30000]
    segment_data = dh.BiSegmentProvider(mollist[:30000], 100, 80, log_transform=True, snr=2.8, log_snr=1.2)
    segment_provider = dh.segment_loader(segment_data, True, 80, 500)
    model = LinearCompressor(length=100, m=0).cpu()
    optimizer = dh.torch.optim.Adam(model.parameters(), lr=1e-3)
    epochs = 25
    model = trainer(model, optimizer, epochs, segment_provider, bilinear=True)
    dh.torch.save(model.state_dict(), MODEL_PATH + "model_3000mols_segment100_overlap80i_nolog_linlog_3000mols_bilinear")


def encode_mols():
    # path_to_mols = "/mnt/local_scratch/akdel001/specific_signals.npy"
    # path_to_mols = "/mnt/local_scratch/akdel001/OptiSpeed/output/good_mols.npy"
    path_to_mols = "/mnt/nexenta/akdel001/phd/Data/eggplant/molecules/run5/run5_mols.npy"
    mollist = np.load(path_to_mols)
    print(mollist.shape)
    # mollist = np.concatenate((mollist, mollist))
    enc = Encoder(LinearCompressor,
                  MODEL_PATH + "eggplant_model",
                  mollist, log_transform=True, length=100, overlap=95, log_snr=1.2)
    enc.encode_segments(labels=True)
    np.save("/mnt/local_scratch/akdel001/OptiSpeed/output/eggplant_segments_only_labels.npy",
            enc.encoded_segments)


if __name__ == "__main__":
    encode_mols()
