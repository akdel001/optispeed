from distutils.core import setup

setup(name='OptiSpeed',
      version='1.0',
      author="Mehmet Akdel",
      packages=["OptiSpeed"],
      install_requires=["numpy", "numba", "torch"],
      # scripts=["bin/caretta-app", "bin/caretta-cli"]
      )
