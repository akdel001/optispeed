import dash
import dash_core_components as dcc
import dash_html_components as html
from OptiScan import utils as bnx
import plotly.graph_objs as go
import numpy as np
from OptiScan import scan, database, utils
from OptiScan.database import MoleculeDB


def get_cum_lens(lengths):
    cumulative_lengths = [0]
    c = 0
    for l in lengths:
        c += l
        cumulative_lengths.append(c)
    return cumulative_lengths


class MQR:
    def __init__(self, output_folder, ref_align_path):
        self.ref_align = ref_align_path
        self.output_dir = output_folder
        self.command = """%s -f -ref %s -i %s -o %s -nosplit 2 -BestRef 1 -biaswt 0 -Mfast 0 -FP 1.5 -FN 0.05 -sf 0.2 -sd 0.0 -A 5 -outlier 1e-3 -outlierMax 40 -endoutlier 1e-4 -S -1000 -sr 0.03 -se 0.2 -MaxSF 0.3 -MaxSE 0.5 -MaxSD 0.12 -resbias 4 64 -maxmem 64 -M 3 3 -minlen 50  -T 1e-10 -maxthreads 12 -hashgen 5 3 2.4 1.5 0.05 5.0 1 1 3 -hash -hashdelta 10 -hashoffset 1 -hashmaxmem 64 -insertThreads 4 -maptype 0 -PVres 2 -PVendoutlier -AlignRes 2.0 -rres 0.9 -resEstimate -ScanScaling 2 -RepeatMask 5 0.01 -RepeatRec 0.7 0.6 1.4 -maxEnd 50 -usecolor 1 -stdout -stderr -subset 1 50000"""
        self.xmap = None

    def run_ref_align(self, reference_cmap_path, bnx_file):
        from subprocess import check_call as ck
        ck(self.command % (self.ref_align, reference_cmap_path, bnx_file, self.output_dir + "bnx_quality"), shell=True)

    def load_results(self):
        xmap_file = self.output_dir + 'bnx_quality.xmap'
        self.xmap = bnx.XmapParser(xmap_file)

external_stylesheets = ["https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.css"]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets, url_base_pathname="/optispeed/")


LENS = np.load("../lens.npy") // 1000
CUM_LENS = get_cum_lens(LENS)
print("length file loaded")
CAS9_SITES = np.load("../cas9_sites.npy") // 1000
CAS9_SITES_NO_OFF = np.load("../cas9_sites_without_off.npy") // 1000
print(CAS9_SITES.shape, CAS9_SITES_NO_OFF.shape)
MQRS = MQR("../mqr_new/", "")
MQRS.load_results()
MQRS.xmap.read_and_load_xmap_file()
MQRS2 = MQR("../new_mqr_750K/", "")
MQRS2.load_results()
MQRS2.xmap.read_and_load_xmap_file()
